FROM nginx:1.19.0

ARG PYTHONPAD_REPO_ID
ARG PYTHONPAD_REPO_PASSWORD
ARG PYTHONPAD_REPO_TAG

ENV INSTALL_PATH /app
RUN mkdir -p $INSTALL_PATH
WORKDIR $INSTALL_PATH

# Install GnuPG2
RUN apt-get update && apt-get install -y gnupg2

# Prepare for installing Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# Install Git, Curl, and Yarn
RUN apt-get update && apt-get install -y git curl yarn
 
# Clone Repo
RUN git clone https://${PYTHONPAD_REPO_ID}:${PYTHONPAD_REPO_PASSWORD}@gitlab.com/intcoding/pythonpad.git .
RUN git reset --hard ${PYTHONPAD_REPO_TAG}

# Install Node dependencies
RUN yarn install --ignore-scripts

# Copy Dependencies 
RUN sh ./scripts/copy-dependencies.sh

# Download and copy Nginx config
RUN curl -SL https://gitlab.com/intcoding/pythonpad-nginx-docker-image/-/raw/master/nginx.conf > /etc/nginx/conf.d/default.conf

# Remove GnuPG2, Git, Curl, and Yarn
RUN apt-get remove -y git curl yarn gnupg2
